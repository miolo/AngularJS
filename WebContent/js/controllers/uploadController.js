angular
		.module("uploadFiles")
		.controller(
				"uploadController",
				function($scope, $http) {

					$scope.porcentagem = 0;
					$scope.classeProgress = "progress-bar progress-bar-info";
					$scope.envMail = function() {
						
						
						
						console.log("Entrou no metodo envio email");
						document.getElementById('loader').style.display = 'block';
						document.getElementById('btnEnviar').style.display = 'none';

						$http.post("http://www.keepupload.com:8095/Cloud/service/mail/contact",
										JSON.stringify({
											"nome" : $scope.email.nome,
											"email" : $scope.email.email,
											"texto" : $scope.email.texto
										})).success(
										function(data, status, headers, config) {
											document.getElementById('statusMsg').style.display = 'block';
											document.getElementById('loader').style.display = 'none';
											document.getElementById('btnEnviar').style.display = 'block';
											$scope.email = {
												nome : '',
												email : '',
												texto : ''
											};
											console.log("Enviou o Email");
										}).error(
										function(data, status, headers, config) {
											document.getElementById('statusMsgErr').style.display = 'block';
											document.getElementById('loader').style.display = 'none';
											document.getElementById('btnEnviar').style.display = 'block';
											console.log("Nao Enviou o email");
										});
					}
					
					$scope.receberEmailVar = {
						to : '',
						patter : '/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i'
					};

					/* Metodo que envia email... é chamado no CustomDropzone.js */
					$scope.receberEmail = function() {
						if ($scope.receberEmailVar.to.length >= 3) {
							$http.post("http://www.keepupload.com:8095/Cloud/service/mail/sendMail",
											JSON.stringify({
														"to" : $scope.receberEmailVar.to,
														"url" : $scope.sdN
											})
										).success(function(data, status, headers,config) {
												console.log("Encaminhou o Email para usuário");
												document.getElementById('inputEmail').value = '';
											}).error(function(data, status, headers,config) {
												console.log("Nao Enviou o email");
												document.getElementById('inputEmail').value = '';
											});
						}

					}
					$scope.senhaDiretorio
					/* Metodo proteje a psta com senha... é chamado no CustomDropzone.js */
					$scope.protegerSenha = function() {
						if ($scope.senhaDiretorio.length >= 6) {
							$http.post("http://www.keepupload.com:8095/Cloud/service/repo/protect",
											JSON.stringify({
														"repoPassword" : $scope.senhaDiretorio,
														"url" : "http://www.keepupload.com/folder.html?key="+$scope.sdN
											})
										).success(function(data, status, headers,config) {
												console.log("Protegeu a pasta com senha!");
												document.getElementById('inputSenha').value = '';
											}).error(function(data, status, headers,config) {
												console.log("Nao protegeu");
												document.getElementById('inputSenha').value = '';
											});
						}

					}
					

					$scope.makeDir = function() {
						$scope.sdN;

						$http.get("http://www.keepupload.com:8095/Cloud/service/repo/new").success(function(data, status) {
									$scope.sdN = data;
						}).error(
						function(data, status, headers, config) {
							$scope.sdN = {};
							alert("Algo deu errado! \nTente novamente mais tarde.");
						});
					}
					
					$scope.closeDir = function() {
						$http.post(
								"http://www.keepupload.com:8095/Cloud/service/repo/done",
								"http://www.keepupload.com/folder.html?key="+ $scope.sdN).success(
										function(data, status, headers, config) {
											// Mostra os arquivos na tela
											console.log("Status " + status);
											$('#myModal').modal('hide');
											$('#secondModal').modal({
												show : true
											});
										}).error(
										function(data, status, headers, config) {
											console.log("Status " + status);
											alert("Você não completou o Upload do seu primeiro arquivo!");
										});
										}
					/*
					 * var consultaWSLogin = function() {
					 * $http.get("http://www.keepupload.com:8095/Cloud/service/auth/5")
					 * .success(function(data, status) { $scope.logado = data; }) };
					 * $scope.email = { nome : '', email : '', texto : '' };
					 */
				});