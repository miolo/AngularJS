
Dropzone.options.myAwesomeDropzone = {
	headers : {
		"Content-Type" : undefined,
	},
	//Aqui pega o retorno que vem depois de fazer o upload de um arquivo.
	success: function(file, response){
		document.getElementById("armazena").innerHTML = 100 - ((response/1024)/1024) + ' MB';
    },
    //Enquanto envia desabilita o botao de upload
    processing: function(){
    	document.getElementById("uploadAll").innerHTML = 'Enviando...';
    	document.getElementById("uploadAll").disabled=true;
    },
    //Quando terminar o envio ele habilita o botão
    
    //Atualiza o total de upload %
    totaluploadprogress: function (uploadProgress, totalBytes, totalBytesSent){
    	if(uploadProgress!= 100){
    		document.getElementById("progress").innerHTML = uploadProgress+'%';
        	document.getElementById("progress").style.width = uploadProgress+'%';
    	} 
    	//alert(uploadProgress);
    },
    //Habilita botão para fechar a pasta
    queuecomplete: function(){
    	/*document.getElementById("nextStep").disabled=false;*/
    	document.getElementById("uploadAll").disabled=false;
    	document.getElementById("uploadAll").innerHTML = 'Fazer Upload';
    	document.getElementById("nextStep").click();
    	var angularScope = angular.element(document.getElementById('inputEmail')).scope();
    	angularScope.receberEmail();
    	angularScope.protegerSenha();
    	document.getElementById("armazena").innerHTML = 100
    	//zera tudo
    },
	autoProcessQueue: false,
	init: function() {
		var submitButton = document.querySelector("#uploadAll")
		var renovar = document.querySelector("#criarPasta")
	    var myDropzone = this; 
	    //Remove todos os arquivos quando Abre o Modal
		renovar.addEventListener("click", function() {
		      myDropzone.removeAllFiles();
		});
		//Envia todos os arquivos quando clica em Upload
	    submitButton.addEventListener("click", function() {
	    	myDropzone.options.url = "http://www.keepupload.com:8095/Cloud/service/repo/"+document.getElementById("chave").value;
	      myDropzone.processQueue();
	      
	    });
	},
	maxFilesize: 100,
	parallelUploads: 100,
	//url : "http://www.keepupload.com:8095/Cloud/service/repo/",
	dictDefaultMessage : "Clique e arraste seus arquivos pra cá",
	dictFallbackMessage : "Seu navegador não suporte a ferramenta de arrastar.",
	dictFallbackText : "Please use the fallback form below to upload your files like in the olden days.",
	dictFileTooBig : "O arquivo é muito grande ({{filesize}}MiB). Tamanho maximo permitido: {{maxFilesize}}MiB.",
	dictInvalidFileType : "Você não pode enviar arquivos desse formato.",
	dictResponseError : "Erro ao enviar: {{statusCode}}.",
	dictCancelUpload : "Cancelar Upload",
	dictCancelUploadConfirmation : "Tem certeza que deseja cancelar o Upload?",
	dictRemoveFile : "Remover arquivo",
	dictRemoveFileConfirmation : false,
	dictMaxFilesExceeded : "Você não pode enviar mais arquivos.",
	//Tem que deixar se não o metodo REMOVEALLFILES() nao funciona!
	addRemoveLinks : true,
	uploadMultiple : false

};

